import pybreaker
import logging
import datetime
import time
from http_server_mock import HttpServerMock
from requests import *

max_failures = 5
Timeout = float(10)
cbreaker = pybreaker.CircuitBreaker(fail_max=max_failures, reset_timeout=Timeout)

@cbreaker
def post(url, data=None, json=None, **kwargs):
    r"""Sends a POST request.

    :param url: URL for the new :class:`Request` object.
    :param data: (optional) Dictionary, list of tuples, bytes, or file-like
        object to send in the body of the :class:`Request`.
    :param json: (optional) A JSON serializable Python object to send in the body of the :class:`Request`.
    :param \*\*kwargs: Optional arguments that ``request`` takes.
    :return: :class:`Response <Response>` object
    :rtype: requests.Response
    """

    return request("post", url, data=data, json=json, **kwargs)

@cbreaker
def get(url, params=None, **kwargs):
    r"""Sends a GET request.

    :param url: URL for the new :class:`Request` object.
    :param params: (optional) Dictionary, list of tuples or bytes to send
        in the query string for the :class:`Request`.
    :param \*\*kwargs: Optional arguments that ``request`` takes.
    :return: :class:`Response <Response>` object
    :rtype: requests.Response
    """

    return request("get", url, params=params, **kwargs)

"""output of circuitbreaker parameter like cooldown, state etc."""
def print_summary():
    name = cbreaker.name
    state = cbreaker._state_storage.state
    time_left = Timeout
    if cbreaker._state_storage.opened_at:
        elapse_time = datetime.datetime.utcnow() - cbreaker._state_storage.opened_at
        time_left = Timeout - elapse_time.total_seconds()
    print("{} circuit state: {}. Time till circuit closed: {}".format(name, state, time_left))
