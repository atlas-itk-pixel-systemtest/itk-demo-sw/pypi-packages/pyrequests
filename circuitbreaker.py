import pybreaker
import logging
import datetime
import requests
import time
from http_server_mock import HttpServerMock
import requests
# from service_registry.adapter.etcd_adapter import ServiceRegistry

# # import redis
# sr = ServiceRegistry(namespace="demi")
# etcd = etcd3gw.client("daqdev11.detlab.lan", api_path="/v3/")
# # redis = redis.StrictRedis()
# # cbreaker = pybreaker.CircuitBreaker(
# #     fail_max= max_fail,
# #     reset_timeout= Timeout,
# #     state_storage=pybreaker.CircuitRedisStorage(pybreaker.STATE_CLOSED, redis))


# """Class that be can used to execute certain code or a callback , if one of the following events(before_call,state_change,failure,success) happens"""
# class CBreaker_Listener(pybreaker.CircuitBreakerListener):
#     "Listener used by circuit breakers that execute database operations."

#     def before_call(self, cb, func, *args, **kwargs):
#         "Called before the circuit breaker `cb` calls `func`."
#         pass

#     def state_change(self, cb, old_state, new_state):
#         "Called when the circuit breaker `cb` state changes."
#         print(old_state.name,"-->",new_state.name)
#         pass

#     def failure(self, cb, exc):
#         "Called when a function invocation raises a system error."
#         pass

#     def success(self, cb):
#         "Called when a function invocation succeeds."
#         pass

# """Listener used to log circuit breaker events."""
# class LogListener(pybreaker.CircuitBreakerListener):
#     def state_change(self, cb, old_state, new_state):
#         msg = "State Change: CB: {0}, New State: {1}".format(cb.name, new_state)
#         logging.info(msg)


"""The actual circuitbreaker with the main parameters"""
max_failures = 5
Timeout = 10
cbreaker = pybreaker.CircuitBreaker(fail_max=max_failures, reset_timeout=Timeout)

"""get function with circuitbreaker decorator; main part of the circuitbreaker"""
@cbreaker
def get_greeting(url, port):
    ''' Get the main index page response '''
    response = requests.get("http://{}:{}/".format(url, port))
    return response.text


"""output of circuitbreaker parameter like cooldown, state etc."""
def print_summary():
    name = cbreaker.name
    state = cbreaker._state_storage.state
    time_left = Timeout
    if cbreaker._state_storage.opened_at:
        elapse_time = datetime.datetime.utcnow() - cbreaker._state_storage.opened_at
        time_left = Timeout - elapse_time.total_seconds()
    print("{} circuit state: {}. Time till circuit closed: {}".format(name, state, time_left))


"""simple Server that can be called and shutdown if needed"""
app = HttpServerMock(__name__)


def index():
    ''' Main index page endpoint '''
    return "Hello world!"


"""a test function trying to get information from a local server"""
def greetings_test(iterations=10, delay=1, url="localhost", port=5000):
    for i in range(iterations): 
        try: 
            get_greeting(url, port) 
        except: 
            pass
 
        print_summary()
        time.sleep(delay) 


""" small setup to start server and show the basics of the circuitbreaker"""
if __name__ == "__main__":
    print("Server is turned OFF...") 
    greetings_test()
         
    print("Server is turning ON...") 
    with app.run("localhost", 5000):
        greetings_test()