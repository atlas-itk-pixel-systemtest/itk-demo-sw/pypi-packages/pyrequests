import pyrequests.requests as prq
import time
from http_server_mock import HttpServerMock

def test_server_off():
    for i in range(10): 
        if i >=5:
            assert prq.cbreaker._state_storage.state == "open"
        elif i < 5:
            assert prq.cbreaker._state_storage.state == "closed"

        try: 
            response = prq.get("http://localhost:5000/")
        except:
            pass

def test_server_switch():
    for i in range(10): 
        if i >=5:
            assert prq.cbreaker._state_storage.state == "open" 
        elif i < 5:
            assert prq.cbreaker._state_storage.state == "closed"

        try: 
            resp = prq.get("http://localhost:5000/")
        except:
            pass

        time.sleep(1)

    with HttpServerMock(__name__).run("localhost", 5000):
        for i in range(10):
            if i >=5:
                assert prq.cbreaker._state_storage.state == "closed"
            elif i < 5:
                assert prq.cbreaker._state_storage.state == "open"
            try: 
                resp = prq.get("http://localhost:5000/")
                assert resp.status_code == 404
            except:
                pass
            time.sleep(1)

def test_server_on():
    with HttpServerMock(__name__).run("localhost", 5000):
        for i in range(20):
            assert prq.cbreaker._state_storage.state == "closed"
            resp = prq.get("http://localhost:5000/")
            assert resp.status_code == 404

#if __name__ == "__main__":
#    test_server_switch()